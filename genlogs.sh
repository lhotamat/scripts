#!/bin/bash


# GET IDs
echo -n "How many IDs? <1-1,000,000>: "
read n

while [ $n -lt 1 ]
do
	echo "Too small, try again: "
	read n
done

while [ $n -gt 100000 ]
do
	echo "Too big, try again: "
	read n
done


# GET MAX ID
echo -n "Biggest number: "
read nmax

while [ $nmax -lt 0 ]
do
	echo "Too small, try again: "
	read nmax
done

while [ $nmax -gt 99999 ]
do
	echo "Too big, try again: "
	read nmax
done


# GET QUERIES
echo -n "How many queries?: "
read m

while [ $m -lt 1 ]
do
	echo "Too small, try again: "
	read m
done

while [ $m -gt 1000000 ]
do
	echo "Too big, try again: "
	read m
done


# GENERATE IDs AND QUERYs
touch tmp

while [ $n -gt 0 ]
do
	echo '+' $(( RANDOM % $nmax )) >> tmp
	(( n-- ))
done

while [ $m -gt 0 ]
do
	start=$(( ( RANDOM + (RANDOM % 20) )  % nmax ))
	finish=$(( RANDOM % nmax ))
	while [ $start -gt $finish ]
	do
		start=$(( ( RANDOM + (RANDOM % 20) )  % nmax ))
		finish=$(( RANDOM % nmax ))
	done

	echo '?' $start $finish >> tmp

	(( m-- ))
done


# GET DESTINATION NAME
echo -n "Name of the data file: "

read filename
while [ -f $filename ]
do
	echo -n "File already exists, overwrite? [y/n]: "
	read yn
	while [ $yn != 'y' -a $yn != 'n' ]
	do
		echo -n "[y/n]: "
		read yn
	done

	if [ $yn = 'y' ]
	then
		break
	else
		echo -n "File: "
		read filename
	fi
done

mv tmp $filename
